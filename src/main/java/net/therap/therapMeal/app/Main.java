package net.therap.therapMeal.app;

import net.therap.therapMeal.configure.DatabaseConfigure;
import net.therap.therapMeal.helper.UserRequestHelper;

import java.sql.*;

/**
 * @author rakibul.hasan
 * @since 2/11/20
 */
public class Main {

    public static void main(String[] args) throws Exception {
        DatabaseConfigure dbConfigure = new DatabaseConfigure();
        Connection dbConnection = dbConfigure.setupDBConnection();

        UserRequestHelper requestHelper = new UserRequestHelper(dbConnection);
        requestHelper.serveUser();

        dbConfigure.closeDBConnection();
    }
}
