package net.therap.therapMeal.dao;

import net.therap.therapMeal.domain.Item;

import java.sql.*;
import java.util.*;

/**
 * @author rakibul.hasan
 * @since 2/16/20
 */
public class ItemDAO {

    private Connection dbConnection;

    public ItemDAO(Connection dbConnection) {
        this.dbConnection = dbConnection;
    }

    public ResultSet getItems() throws SQLException {
        String query = "SELECT * FROM Item;";

        Statement statement = dbConnection.createStatement();

        ResultSet result = statement.executeQuery(query);

        return result;
    }

    public void insertItem(Item item) throws SQLException {
        String query = "INSERT INTO Item(Name) VALUES(?);";

        PreparedStatement preparedStatement = dbConnection.prepareStatement(query);

        preparedStatement.setString(1, item.getName());
        preparedStatement.executeUpdate();
    }

    public void deleteItem(Item item) throws SQLException {
        String query = "DELETE FROM Item WHERE Id = ?;";

        PreparedStatement preparedStatement = dbConnection.prepareStatement(query);

        preparedStatement.setInt(1, item.getId());
        preparedStatement.executeUpdate();
    }
}
