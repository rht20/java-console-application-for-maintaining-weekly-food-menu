package net.therap.therapMeal.dao;

import net.therap.therapMeal.domain.Meal;

import java.sql.*;
import java.util.*;

/**
 * @author rakibul.hasan
 * @since 2/16/20
 */
public class MealDAO {

    private Connection dbConnection;

    public MealDAO(Connection dbConnection) {
        this.dbConnection = dbConnection;
    }

    public ResultSet getMeals() throws SQLException {
        String query = "SELECT * FROM Meal;";

        Statement statement = dbConnection.createStatement();

        ResultSet result = statement.executeQuery(query);

        return result;
    }

    public void insertMeal(Meal meal) throws SQLException {
        String query = "INSERT INTO Meal(Name) VALUES(?);";

        PreparedStatement preparedStatement = dbConnection.prepareStatement(query);

        preparedStatement.setString(1, meal.getName());
        preparedStatement.executeUpdate();
    }

    public void deleteMeal(Meal meal) throws SQLException {
        String query = "DELETE FROM Meal WHERE Id = ?;";

        PreparedStatement preparedStatement = dbConnection.prepareStatement(query);

        preparedStatement.setInt(1, meal.getId());
        preparedStatement.executeUpdate();
    }
}
