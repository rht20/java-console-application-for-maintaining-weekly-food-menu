package net.therap.therapMeal.dao;

import net.therap.therapMeal.domain.*;

import java.sql.*;
import java.util.*;

/**
 * @author rakibul.hasan
 * @since 2/16/20
 */
public class MenuDAO {

    private Connection dbConnection;

    public MenuDAO(Connection dbConnection) {
        this.dbConnection = dbConnection;
    }

    public ResultSet getMenu(Meal meal, Day day) throws SQLException {
        String query = "SELECT Item_Id FROM Menu WHERE Meal_Id = ? AND Day_Id = ?;";

        PreparedStatement preparedStatement = dbConnection.prepareStatement(query);

        preparedStatement.setInt(1, meal.getId());
        preparedStatement.setInt(2, day.ordinal());

        ResultSet result = preparedStatement.executeQuery();

        return result;
    }

    public void insertMenu(Meal meal, Day day, Item item) throws SQLException {
        String query = "INSERT INTO Menu VALUES(?, ?, ?);";

        PreparedStatement preparedStatement = dbConnection.prepareStatement(query);

        preparedStatement.setInt(1, meal.getId());
        preparedStatement.setInt(2, day.ordinal());
        preparedStatement.setInt(3, item.getId());

        preparedStatement.executeUpdate();
    }

    public void deleteByItemId(Item item) throws SQLException {
        String query = "DELETE FROM Menu WHERE Item_Id = ?;";

        PreparedStatement preparedStatement = dbConnection.prepareStatement(query);

        preparedStatement.setInt(1, item.getId());
        preparedStatement.executeUpdate();
    }

    public void deleteByMealId(Meal meal) throws SQLException {
        String query = "DELETE FROM Menu WHERE Meal_Id = ?;";

        PreparedStatement preparedStatement = dbConnection.prepareStatement(query);

        preparedStatement.setInt(1, meal.getId());
        preparedStatement.executeUpdate();
    }

    public void deleteMenuItem(Meal meal, Day day, Item item) throws SQLException {
        String query = "DELETE FROM Menu WHERE Meal_Id = ? AND Day_Id = ? AND Item_Id = ?;";

        PreparedStatement preparedStatement = dbConnection.prepareStatement(query);

        preparedStatement.setInt(1, meal.getId());
        preparedStatement.setInt(2, day.ordinal());
        preparedStatement.setInt(3, item.getId());
        preparedStatement.executeUpdate();
    }
}
