package net.therap.therapMeal.configure;

import java.sql.*;

/**
 * @author rakibul.hasan
 * @since 2/13/20
 */
public class DatabaseConfigure {

    Connection dbConnection;

    public Connection setupDBConnection() throws Exception {
        String url = "jdbc:mysql://127.0.0.1:3306/TherapMeal";
        String userName = "rakibul.hasan";
        String password = "therap";

        Class.forName("com.mysql.jdbc.Driver");
        dbConnection = DriverManager.getConnection(url, userName, password);

        return dbConnection;
    }

    public void closeDBConnection() throws Exception {
        dbConnection.close();
    }
}
