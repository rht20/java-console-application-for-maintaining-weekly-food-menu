package net.therap.therapMeal.service;

import net.therap.therapMeal.dao.*;
import net.therap.therapMeal.domain.*;
import net.therap.therapMeal.helper.*;

import java.sql.*;
import java.util.*;

/**
 * @author rakibul.hasan
 * @since 2/13/20
 */
public class MenuService {

    private Connection dbConnection;

    public MenuService(Connection dbConnection) {
        this.dbConnection = dbConnection;
    }

    public void viewMenu() throws SQLException {
        PrintHelper printHelper = new PrintHelper();

        MenuDAO menuDAO = new MenuDAO(dbConnection);

        Meal meal = getMealInfo();
        Day day = getDayInfo();

        ResultSet result = menuDAO.getMenu(meal, day);

        Menu menu = processResult(meal, day, result);

        printHelper.printMenu(menu);
    }

    public void addItemToMenu() throws SQLException {
        MenuDAO menuDAO = new MenuDAO(dbConnection);

        Meal meal = getMealInfo();
        Day day = getDayInfo();
        Item item = getItemInfo();

        menuDAO.insertMenu(meal, day, item);
    }

    public void removeItemFromMenu() throws SQLException {
        InputHelper inputHelper = new InputHelper();
        PrintHelper printHelper = new PrintHelper();

        MenuDAO menuDAO = new MenuDAO(dbConnection);

        Meal meal = getMealInfo();
        Day day = getDayInfo();

        ResultSet result = menuDAO.getMenu(meal, day);

        Menu menu = processResult(meal, day, result);

        printHelper.printItemList(menu.getItemList());

        printHelper.printString("Enter item id: ");
        int id = inputHelper.inputInteger();
        Item item = new Item(id, null);

        menuDAO.deleteMenuItem(meal, day, item);
    }

    public Menu processResult(Meal meal, Day day, ResultSet result) throws SQLException {
        Menu menu = new Menu(meal, day, new ArrayList<>());

        Map<Integer, String> map = getItemIdToItemNameMapping();

        while (result.next()) {
            int itemId = result.getInt("Item_Id");
            Item item = new Item(itemId, map.get(itemId));
            menu.getItemList().add(item);
        }

        return menu;
    }

    public Item getItemInfo() throws SQLException {
        InputHelper inputHelper = new InputHelper();
        PrintHelper printHelper = new PrintHelper();

        ItemDAO itemDAO = new ItemDAO(dbConnection);

        ItemService itemService = new ItemService(dbConnection);

        ResultSet result = itemDAO.getItems();

        List<Item> itemList = itemService.processResult(result);

        printHelper.printItemList(itemList);

        printHelper.printString("Enter item id: ");
        int id = inputHelper.inputInteger();

        return itemList.get(id - 1);
    }

    public Meal getMealInfo() throws SQLException {
        InputHelper inputHelper = new InputHelper();
        PrintHelper printHelper = new PrintHelper();

        MealDAO mealDAO = new MealDAO(dbConnection);

        MealService mealService = new MealService(dbConnection);

        ResultSet result = mealDAO.getMeals();

        List<Meal> mealList = mealService.processResult(result);

        printHelper.printMealList(mealList);

        printHelper.printString("Enter meal id: ");
        int id = inputHelper.inputInteger();

        return mealList.get(id - 1);
    }

    public Day getDayInfo() {
        InputHelper inputHelper = new InputHelper();
        PrintHelper printHelper = new PrintHelper();

        printHelper.printDayList();

        printHelper.printString("Enter day id: ");
        int id = inputHelper.inputInteger();

        return Day.values()[id - 1];
    }

    public Map<Integer, String> getItemIdToItemNameMapping() throws SQLException {
        ItemDAO itemDAO = new ItemDAO(dbConnection);

        ItemService itemService = new ItemService(dbConnection);

        ResultSet result = itemDAO.getItems();

        List<Item> itemList = itemService.processResult(result);

        Map<Integer, String> map = new HashMap<>();

        for (Item item : itemList) {
            map.put(item.getId(), item.getName());
        }

        return map;
    }
}
