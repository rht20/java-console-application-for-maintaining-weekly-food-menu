package net.therap.therapMeal.service;

import net.therap.therapMeal.dao.MealDAO;
import net.therap.therapMeal.dao.MenuDAO;
import net.therap.therapMeal.domain.Meal;
import net.therap.therapMeal.helper.*;

import java.sql.*;
import java.util.*;

/**
 * @author rakibul.hasan
 * @since 2/11/20
 */
public class MealService {

    private Connection dbConnection;

    public MealService(Connection dbConnection) {
        this.dbConnection = dbConnection;
    }

    public void viewMealTypes() throws SQLException {
        PrintHelper printHelper = new PrintHelper();

        MealDAO mealDAO = new MealDAO(dbConnection);

        ResultSet result = mealDAO.getMeals();

        List<Meal> mealList = processResult(result);

        printHelper.printMealList(mealList);
    }

    public void addNewMealType() throws SQLException {
        InputHelper inputHelper = new InputHelper();
        PrintHelper printHelper = new PrintHelper();

        MealDAO mealDAO = new MealDAO(dbConnection);

        printHelper.printString("Enter type name: ");
        String typeName = inputHelper.inputString();

        Meal meal = new Meal(0, typeName);

        mealDAO.insertMeal(meal);
    }

    public void removeMealType() throws SQLException {
        MealDAO mealDAO = new MealDAO(dbConnection);
        MenuDAO menuDAO = new MenuDAO(dbConnection);

        Meal meal = getMealInfo();

        menuDAO.deleteByMealId(meal);
        mealDAO.deleteMeal(meal);
    }

    public List<Meal> processResult(ResultSet result) throws SQLException {
        List<Meal> mealList = new ArrayList<>();

        while (result.next()) {
            Meal meal = new Meal(result.getInt("Id"), result.getString("Name"));
            mealList.add(meal);
        }

        return mealList;
    }

    public Meal getMealInfo() throws SQLException {
        InputHelper inputHelper = new InputHelper();
        PrintHelper printHelper = new PrintHelper();

        MealDAO mealDAO = new MealDAO(dbConnection);

        ResultSet result = mealDAO.getMeals();

        List<Meal> mealList = processResult(result);

        printHelper.printMealList(mealList);

        printHelper.printString("Enter meal id: ");
        int id = inputHelper.inputInteger();

        return mealList.get(id - 1);
    }
}
