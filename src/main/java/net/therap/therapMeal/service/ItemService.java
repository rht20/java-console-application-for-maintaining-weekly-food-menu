package net.therap.therapMeal.service;

import net.therap.therapMeal.dao.ItemDAO;
import net.therap.therapMeal.dao.MenuDAO;
import net.therap.therapMeal.domain.Item;
import net.therap.therapMeal.helper.*;

import java.sql.*;
import java.util.*;

/**
 * @author rakibul.hasan
 * @since 2/13/20
 */
public class ItemService {

    private Connection dbConnection;

    public ItemService(Connection dbConnection) {
        this.dbConnection = dbConnection;
    }

    public void viewItems() throws SQLException {
        PrintHelper printHelper = new PrintHelper();

        ItemDAO itemDAO = new ItemDAO(dbConnection);

        ResultSet result = itemDAO.getItems();

        List<Item> itemList = processResult(result);

        printHelper.printItemList(itemList);
    }

    public void addNewItem() throws SQLException {
        InputHelper inputHelper = new InputHelper();
        PrintHelper printHelper = new PrintHelper();

        ItemDAO itemDAO = new ItemDAO(dbConnection);

        printHelper.printString("Enter item name: ");
        String itemName = inputHelper.inputString();

        Item item = new Item(0, itemName);

        itemDAO.insertItem(item);
    }

    public void removeItem() throws SQLException {
        MenuDAO menuDAO = new MenuDAO(dbConnection);

        ItemDAO itemDAO = new ItemDAO(dbConnection);

        Item item = getItemInfo();

        menuDAO.deleteByItemId(item);
        itemDAO.deleteItem(item);
    }

    public List<Item> processResult(ResultSet result) throws SQLException {
        List<Item> itemList = new ArrayList<>();

        while (result.next()) {
            Item item = new Item(result.getInt("Id"), result.getString("Name"));
            itemList.add(item);
        }

        return itemList;
    }

    public Item getItemInfo() throws SQLException {
        InputHelper inputHelper = new InputHelper();
        PrintHelper printHelper = new PrintHelper();

        ItemDAO itemDAO = new ItemDAO(dbConnection);

        ResultSet result = itemDAO.getItems();

        List<Item> itemList = processResult(result);

        printHelper.printItemList(itemList);

        printHelper.printString("Enter item id: ");
        int id = inputHelper.inputInteger();

        return itemList.get(id - 1);
    }
}
