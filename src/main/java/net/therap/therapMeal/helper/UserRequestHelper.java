package net.therap.therapMeal.helper;

import net.therap.therapMeal.service.*;

import java.sql.*;

/**
 * @author rakibul.hasan
 * @since 2/13/20
 */
public class UserRequestHelper {

    private Connection dbConnection;

    public UserRequestHelper(Connection dbConnection) {
        this.dbConnection = dbConnection;
    }

    public void serveUser() throws Exception {
        PrintHelper printHelper = new PrintHelper();

        while (true) {
            printHelper.printMainMenu();

            if (chooseMainMenuOption()) {
                break;
            }
        }
    }

    public boolean chooseMainMenuOption() throws Exception {
        InputHelper inputHelper = new InputHelper();
        PrintHelper printHelper = new PrintHelper();

        ItemService itemService = new ItemService(dbConnection);
        MealService mealService = new MealService(dbConnection);
        MenuService menuService = new MenuService(dbConnection);

        printHelper.printString("Enter option id: ");
        int option = inputHelper.inputInteger();

        switch (option) {
            case 1:
                itemService.viewItems();
                break;
            case 2:
                mealService.viewMealTypes();
                break;
            case 3:
                menuService.viewMenu();
                break;
            case 4:
                itemService.addNewItem();
                break;
            case 5:
                itemService.removeItem();
                break;
            case 6:
                mealService.addNewMealType();
                break;
            case 7:
                mealService.removeMealType();
                break;
            case 8:
                menuService.addItemToMenu();
                break;
            case 9:
                menuService.removeItemFromMenu();
                break;
        }

        return (option == 10);
    }
}
