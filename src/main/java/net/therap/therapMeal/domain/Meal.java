package net.therap.therapMeal.domain;

/**
 * @author rakibul.hasan
 * @since 2/13/20
 */
public class Meal {

    private int id;
    private String name;

    public Meal(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
