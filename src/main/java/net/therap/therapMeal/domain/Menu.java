package net.therap.therapMeal.domain;

import java.util.*;

/**
 * @author rakibul.hasan
 * @since 2/13/20
 */
public class Menu {

    private Meal meal;
    private Day day;
    private List<Item> itemList;

    public Menu(Meal meal, Day day, List<Item> itemList) {
        this.meal = meal;
        this.day = day;
        this.itemList = itemList;
    }

    public void setMeal(Meal meal) {
        this.meal = meal;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public void setItemList(List<Item> itemList) {
        this.itemList = itemList;
    }

    public Meal getMeal() {
        return meal;
    }

    public Day getDay() {
        return day;
    }

    public List<Item> getItemList() {
        return itemList;
    }
}
